<!DOCTYPE html>
<html>
  <meta charset="utf-8" />
  <head>
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />
    <link rel="stylesheet" href="../../resources/css/style.css">
  </head>
  <body>
    
    <div id="map"></div>


    <!--JS-->
    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>
    <script src= "../../resources/js/index.js"></script>
    
  </body>
</html>