<!DOCTYPE html>
<html>
  <meta charset="utf-8" />
  <head>
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />
    <link rel="stylesheet" href="../../../resources/css/style.css">
  </head>
  <body>
    
    <div id="map"></div>
    <form action = "{{ url('/rutas')}}" >
            
            <button type="submit" class="btn btn-primary">Menu</button>
            </form>
  
    <!--JS-->
    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>
    <script type="text/javascript" src= "../../../resources/js/javaDisplay.js"></script>
    
  </body>
</html>