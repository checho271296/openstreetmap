<!DOCTYPE html>


<html>
  <meta charset="utf-8" />
  <head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/css/style.css">
  </head>
  <body>
    <div class="container ">
    <div class = "row  ">
      <div class="col-12 d-flex justify-content-center">
        <h1>Open Street Map Exercise</h1>
      </div>
    </div>
    <br>
    <div class="row  ">
      <div class="col-6 d-flex justify-content-center">
        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
         <div class="card-header">Buscar  Ruta</div>
          <div class="card-body">
            <h5 class="card-title">Buscar rutas por nombre</h5>
            <p class="card-text">En esta opcion podra buscar sus rutas creadas o rutas creadas por otras personas</p>
            <form action="{{url('/rutas/aa')}}">
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre de la ruta</label>
              <input type="text" class="form-control" id="route" name = "nameRoute">
            </div>
            <button type="submit" class="btn btn-secondary ">Buscar</button>
          </form>
          </div>
        </div>
      </div>
      <div class="col-6 d-flex justify-content-center">
        <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
          <div class="card-header">Crear ruta</div>
          <div class="card-body ">
            <h5 class="card-title">Crear tus propias rutas</h5>
            <p class="card-text">En esta opcion podra crear su propia ruta con un nombre, descripcion y los puntos que desee.</p>
            <form action="{{url('/rutas/create')}}" class= "d-flex justify-content-center">
              <button type="submit" class="btn btn-primary ">Crear Ruta</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">

        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
         <div class="card-header">LABORATORIO 4</div>
          <div class="card-body">
            <h5 class="card-title">Buscar Rutas en por cantidad de puntos</h5>
            <p class="card-text">En esta opcion podra buscar sus rutas creadas con la misma cantidad de puntos</p>
            <form action="{{url('/rutas/searchNodos/edit')}}">
            <div class="form-group">
              <label for="cantidadPuntos">Cantidad de puntos</label>
              <input type="number" class="form-control"  name = "cantidadPuntos">
            </div>
            <button type="submit" class="btn btn-secondary ">Buscar</button>
          </form>
          </div>
        </div>

      </div>
    </div>
  </div>


  

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>