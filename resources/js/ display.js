var mymap = L.map("map").setView([9.85986, -83.9172], 1);

L.tileLayer(
    "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
    {
        attribution:
            "Map data &copy; " +
            '<a href="https://www.openstreetmap.org/">' +
            "OpenStreetMap" +
            "</a> " +
            "contributors," +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">' +
            "CC-BY-SA" +
            "</a>" +
            ", Imagery © " +
            '<a href="https://www.mapbox.com/">' +
            "Mapbox" +
            "</a>",
        maxZoom: 17,
        id: "mapbox/streets-v11",
        accessToken:
            "pk.eyJ1IjoicHVjdHJ1cyIsImEiOiJjazU1ZW5iYmgwdDdjM2pyNWF0NzFtNm53In0.l1NgBfNq-uWu_3LVsn7v3Q"
    }
).addTo(mymap);

const xhttp = new XMLHttpRequest();
xhttp.open("GET", "../../../resources/json/ruta.json", true);
xhttp.send();

xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        let datos = JSON.parse(this.responseText);
        var cont = 0;
        var starx;
        var starty;

        for (let item of datos) {
            L.Routing.control({
                waypoints: [
                    L.latLng(starx, starty),
                    L.latLng(item.lat, item.long)
                ],
                createMarker: function() {
                    return null;
                },
                routeWhileDragging: false
            }).addTo(mymap);
            if (item.descripcion == "Punto Inicial") {
                var myIcon = L.icon({
                    iconUrl: "../../../resources/images/fourbyfour.png",
                    iconSize: [32, 37],
                    popupAnchor: [0, -14]
                });
                var markerSF = L.marker([item.lat, item.long], {
                    icon: myIcon
                }).addTo(mymap);
                markerSF.bindPopup(item.descripcion);
                starx = item.lat;
                starty = item.long;
            } else if (item.descripcion == "Punto Final") {
                var myIcon = L.icon({
                    iconUrl: "../../../resources/images/home-2.png",
                    iconSize: [32, 37],
                    popupAnchor: [0, -14]
                });
                var markerSF = L.marker([item.lat, item.long], {
                    icon: myIcon
                }).addTo(mymap);
                markerSF.bindPopup(item.descripcion);
                starx = undefined;
                starty = undefined;
            } else {
                var markerSF = L.marker([item.lat, item.long]).addTo(mymap);
                markerSF.bindPopup(
                    "Ruta: ".concat(item.nombreRuta, ", ", item.descripcion)
                );
                starx = item.lat;
                starty = item.long;
            }
        }
    }
};
