var mymap = L.map("map").setView([9.85986, -83.9172], 10);

L.tileLayer(
    "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
    {
        attribution:
            "Map data &copy; " +
            '<a href="https://www.openstreetmap.org/">' +
            "OpenStreetMap" +
            "</a> " +
            "contributors," +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">' +
            "CC-BY-SA" +
            "</a>" +
            ", Imagery © " +
            '<a href="https://www.mapbox.com/">' +
            "Mapbox" +
            "</a>",
        maxZoom: 17,
        id: "mapbox/streets-v11",
        accessToken:
            "pk.eyJ1IjoicHVjdHJ1cyIsImEiOiJjazU1ZW5iYmgwdDdjM2pyNWF0NzFtNm53In0.l1NgBfNq-uWu_3LVsn7v3Q"
    }
).addTo(mymap);
var arregloPuntos = [];
function onMapClick(e) {
    L.marker([e.latlng.lat, e.latlng.lng]).addTo(mymap);
    arregloPuntos.push([e.latlng.lat, e.latlng.lng]);
    console.log(arregloPuntos);
    document.getElementById("routes").value = JSON.stringify(arregloPuntos);
}

mymap.on("click", onMapClick);
