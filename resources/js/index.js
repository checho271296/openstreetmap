var mymap = L.map("map").setView([9.85986, -83.9172], 10);

L.tileLayer(
    "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
    {
        attribution:
            "Map data &copy; " +
            '<a href="https://www.openstreetmap.org/">' +
            "OpenStreetMap" +
            "</a> " +
            "contributors," +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">' +
            "CC-BY-SA" +
            "</a>" +
            ", Imagery © " +
            '<a href="https://www.mapbox.com/">' +
            "Mapbox" +
            "</a>",
        maxZoom: 17,
        id: "mapbox/streets-v11",
        accessToken:
            "pk.eyJ1IjoicHVjdHJ1cyIsImEiOiJjazU1ZW5iYmgwdDdjM2pyNWF0NzFtNm53In0.l1NgBfNq-uWu_3LVsn7v3Q"
    }
).addTo(mymap);

const xhttp = new XMLHttpRequest();
xhttp.open("GET", "../../resources/json/ruta.json", true);
xhttp.send();

var puntoInicial = L.icon({
    iconUrl: "../../resources/fourbyfour.png",

    iconSize: [100, 100], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [50, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62], // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var puntoInicial = L.icon({
    iconUrl: "../../resources/images/fourbyfour.png",

    iconSize: [50, 50], // size of the icon
    shadowSize: [25, 25], // size of the shadow
    iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62], // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var puntoFinal = L.icon({
    iconUrl: "../../resources/images/home-2.png",

    iconSize: [50, 50], // size of the icon
    shadowSize: [25, 25], // size of the shadow
    iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62], // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        let datos = JSON.parse(this.responseText);
        var cont = 0;
        var starx = undefined;
        var starty = undefined;

        for (let item of datos) {
            if (item.descricionRuta == "Punto Inicial") {
                L.Routing.control({
                    waypoints: [
                        L.latLng(starx, starty),
                        L.latLng(item.lat, item.lng)
                    ],
                    createMarker: function() {
                        return null;
                    },
                    routeWhileDragging: false
                }).addTo(mymap);
                var markerSF = L.marker([item.lat, item.lng], {
                    icon: puntoInicial
                }).addTo(mymap);
                markerSF.bindPopup(item.descricionRuta);
                starx = item.lat;
                starty = item.lng;
            } else if (item.descricionRuta == "Punto Final") {
                L.Routing.control({
                    waypoints: [
                        L.latLng(starx, starty),
                        L.latLng(item.lat, item.lng)
                    ],
                    createMarker: function() {
                        return null;
                    },
                    routeWhileDragging: false
                }).addTo(mymap);
                var markerSF = L.marker([item.lat, item.lng], {
                    icon: puntoFinal
                }).addTo(mymap);
                markerSF.bindPopup(item.descricionRuta);
                starx = item.lat;
                starty = item.lng;
            } else {
                L.Routing.control({
                    waypoints: [
                        L.latLng(starx, starty),
                        L.latLng(item.lat, item.lng)
                    ],
                    createMarker: function() {
                        return null;
                    },
                    routeWhileDragging: false
                }).addTo(mymap);
                var markerSF = L.marker([item.lat, item.lng]).addTo(mymap);
                markerSF.bindPopup(item.descricionRuta);
                starx = item.lat;
                starty = item.lng;
            }
        }
    }
};
