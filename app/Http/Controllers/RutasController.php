<?php

namespace App\Http\Controllers;

use App\rutas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class RutasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('maps.index') ;//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('maps.createRoute');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$dataRoute = request()->except('_token','routes');
        $nameRoute = request()->nameRoute;
        $description = request()->descripcionRuta;
        $waypoints = request()->routes;
        $myarray = [];
        eval("\$myarray = $waypoints;");
        $dato = [];
        for ($i = 0; $i < sizeof($myarray); $i++) {
            if ($i == 0 ){
                $dato[] = ["nameRoute" => $nameRoute,"descripcionRuta" => "Punto Inicial","lat"=> $myarray[$i][0],"lng"=> $myarray[$i][1]]; 
            rutas::insert($dato);
            $dato = [];
            }
            elseif($i == sizeof($myarray)-1){
                $dato[] = ["nameRoute" => $nameRoute,"descripcionRuta" => "Punto Final","lat"=> $myarray[$i][0],"lng"=> $myarray[$i][1]]; 
                rutas::insert($dato);
                $dato = [];
            }else{

           
            $dato[] = ["nameRoute" => $nameRoute,"descripcionRuta" => $description,"lat"=> $myarray[$i][0],"lng"=> $myarray[$i][1]]; 
            rutas::insert($dato);
            $dato = [];
         }
        }
        return view('maps.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\rutas  $rutas
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $waypoints = rutas::where('nameRoute','=',request()->all())->paginate(15);
        $name = request()->nameRoute;
        if( $waypoints->total() != 0){
            $routesJson = [];
        foreach ($waypoints as $punto)
        {
            $routesJson[] = [
                'id' => $punto->id,
                'nameRoute' => $punto->nameRoute,
                'descricionRuta' => $punto->descripcionRuta,
                'lat' => $punto->lat,
                'lng' => $punto->lng
            ];
        }

        $newJsonString = json_encode($routesJson, JSON_PRETTY_PRINT);
        file_put_contents(base_path('resources/json/ruta.json'), stripslashes($newJsonString));
        return view('maps.route') ;
        }
        else{
            
            return view('maps.error') ;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\rutas  $rutas
     * @return \Illuminate\Http\Response
     */
    public function edit(rutas $rutas)
    {
        $number = request()->cantidadPuntos;
        $data2 = [];
        $puntas = DB::table('rutas')->select('nameRoute')->groupBy('nameRoute')->havingRaw("COUNT(*)=".$number)->get();
        for ($i = 0; $i < sizeof($puntas); $i++){
            $puntos = DB::table('rutas')->where('nameRoute', '=', $puntas[$i]->nameRoute)->get();
            foreach ($puntos as $punto) {
                $data2[] = [
                    'id' => $punto->id,
                    'nameRoute' => $punto->nameRoute,
                    'descricionRuta' => $punto->descripcionRuta,
                    'lat' => $punto->lat,
                    'lng' => $punto->lng
                ];
            }
        }
        $newJsonString = json_encode($data2, JSON_PRETTY_PRINT);

        file_put_contents(base_path('resources/json/ruta.json'), stripslashes($newJsonString));
        return view('maps.display');
        //return $puntas;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\rutas  $rutas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, rutas $rutas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\rutas  $rutas
     * @return \Illuminate\Http\Response
     */
    public function destroy(rutas $rutas)
    {
        //
    }
}
